package me.flyray.bsin.server.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

import me.flyray.bsin.server.domain.ProcessDefinition;

@Repository
@Mapper
public interface ActReProcdefMapper {

    List<ProcessDefinition> getProcessDefinitionPageList(@Param("tenantId") String tenantId,
                                                        @Param("name") String name,
                                                         @Param("key") String key);

}
